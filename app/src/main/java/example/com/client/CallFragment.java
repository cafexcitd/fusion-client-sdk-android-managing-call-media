package example.com.client;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Point;
import android.hardware.Camera;
import android.hardware.camera2.CameraManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alicecallsbob.fcsdk.android.phone.Call;
import com.alicecallsbob.fcsdk.android.phone.CallCreationWithErrorException;
import com.alicecallsbob.fcsdk.android.phone.CallListener;
import com.alicecallsbob.fcsdk.android.phone.CallStatus;
import com.alicecallsbob.fcsdk.android.phone.CallStatusInfo;
import com.alicecallsbob.fcsdk.android.phone.Phone;
import com.alicecallsbob.fcsdk.android.phone.PhoneListener;
import com.alicecallsbob.fcsdk.android.phone.PhoneVideoCaptureResolution;
import com.alicecallsbob.fcsdk.android.phone.PhoneVideoCaptureSetting;
import com.alicecallsbob.fcsdk.android.phone.VideoSurface;
import com.alicecallsbob.fcsdk.android.phone.VideoSurfaceListener;
import com.alicecallsbob.fcsdk.android.uc.UC;

import java.util.List;

/**
 * Created by Nathan on 07/04/2016.
 */
public class CallFragment extends Fragment implements VideoSurfaceListener, CallListener, PhoneListener {

    private VideoSurface remoteSurface = null;
    private VideoSurface localSurface = null;

    // audio / video configuration fields
    private CheckBox audioCheckBox;
    private CheckBox videoCheckBox;
    private CheckBox rearCamCheckBox;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_call, container, false);

        // make the log scrollable & clear any lorem ipsum text
        TextView logView = (TextView) view.findViewById(R.id.log);
        logView.setMovementMethod(new ScrollingMovementMethod());
        logView.setText("");

        // wire up the local audio / video & camera change check boxes
        this.audioCheckBox = (CheckBox)view.findViewById(R.id.audio);
        this.videoCheckBox = (CheckBox)view.findViewById(R.id.video);
        this.rearCamCheckBox = (CheckBox)view.findViewById(R.id.rear_cam);

        // wire up the dial / hangup
        Button dialButton = (Button) view.findViewById(R.id.dial);
        final CallListener callListener = this;
        dialButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View button) {
                // hide the keyboard
                hideKeyboard();

                // find the address the user dialled
                EditText addressField = (EditText) view.findViewById(R.id.address);
                String address = addressField.getText().toString();


                // ensure that the video views are set up
                setupVideoViews(view);

                try {
                    // determine the media capabilities
                    boolean audio = audioCheckBox.isChecked();
                    boolean video = videoCheckBox.isChecked();

                    // create the call
                    Call call = ConfigFragment.UC.getPhone().createCall(address, audio, video, callListener);

                    // determine where the remote video stream should be displayed
                    call.setVideoView(remoteSurface);
                } catch (Exception e) {
                    e.printStackTrace();
                    addMessageToLog("Error: " + e.getMessage());
                }

            }
        });

        Button hangupButton = (Button) view.findViewById(R.id.hangup);
        hangupButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View button) {
                // hide the keyboard
                hideKeyboard();

                // hangup a call (if we're on one)
                Call[] calls = ConfigFragment.UC.getPhone().getCurrentCalls().toArray(new Call[0]);
                for(Call call : calls) {
                    call.end();
                }

                // now make the local / remote views invisible
                // nb - it is valid in this implementation to pass through null as the call
                // object since our implementation does not use the call object for anything
                onStatusChanged(null, CallStatus.ENDED);

                // add the message to the log
                addMessageToLog("User clicked to hangup");
            }
        });

        this.audioCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton button, boolean checked) {
                addMessageToLog("Enable audio: " + checked);
                ConfigFragment.UC.getPhone().enableLocalAudio(checked);

            }
        });

        this.videoCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton button, boolean checked) {
                addMessageToLog("Enable video: " + checked);
                ConfigFragment.UC.getPhone().enableLocalVideo(checked);
            }
        });

        this.rearCamCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton button, boolean checked) {
                addMessageToLog("Use rear cam: " + checked);

                // determine which cam should be used
                int cam = checked
                    ? Camera.CameraInfo.CAMERA_FACING_BACK
                    : Camera.CameraInfo.CAMERA_FACING_FRONT;

                // update the Phone
                ConfigFragment.UC.getPhone().setCamera(cam);
            }
        });

        // return the prepared statement
        return view;
    }

    // adds a message to the log
    private void addMessageToLog(final String message) {
        final TextView textView = (TextView) this.getView().findViewById(R.id.log);
        this.getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                textView.setText(message + "\n" + textView.getText());
            }
        });
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getView().getWindowToken(), 0);
    }



    //////////////////////////////////////////
    //
    // Ensures that the video views are set up

    private void setupVideoViews(View view) {

        int width = 640;
        int height = 480;
        Point remoteDimensions = new Point(width, height);
        Point localDimensions = new Point(width / 4, height / 4);

        // construct the VideoSurface's
        Phone phone = ConfigFragment.UC.getPhone();
        this.remoteSurface = phone.createVideoSurface(this.getActivity(), remoteDimensions, this);
        this.localSurface = phone.createVideoSurface(this.getActivity(), localDimensions, this);

        // get references to the local / remote video surface containers in the UI
        RelativeLayout remoteContainer = (RelativeLayout) view.findViewById(R.id.remote);
        RelativeLayout localContainer = (RelativeLayout) view.findViewById(R.id.local);

        // empty the views
        remoteContainer.removeAllViews();
        localContainer.removeAllViews();

        // add the VideoSurface's to the UI
        remoteContainer.addView(remoteSurface);
        localContainer.addView(localSurface);

        // ensure that the containers are visible
        remoteContainer.setVisibility(View.VISIBLE);
        localContainer.setVisibility(View.VISIBLE);

        // now set the preview view on the phone
        phone.setPreviewView(localSurface);
        phone.setCamera(Camera.CameraInfo.CAMERA_FACING_FRONT);

        // adjust the image orientation!
        phone.setVideoOrientation(270);
    }




    //////////////////////////////////////////
    //
    // VideoSurfaceListener methods

    @Override
    public void onFrameSizeChanged(final int width, final int height,
                                   final VideoSurface.Endpoint endpoint, final VideoSurface surface)
    {
        this.getActivity().runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                // determine the appropriate size of the view area
                Point point = endpoint == VideoSurface.Endpoint.LOCAL
                    ? new Point(width / 4, height / 4)
                    : new Point(width, height);


                surface.setDimensions(point);
            }
        });
    }

    @Override
    public void onSurfaceRenderingStarted(VideoSurface videoSurface) {
        this.addMessageToLog("onSurfaceRenderingStarted");
    }



    //////////////////////////////////////////
    //
    // Call Listener methods

    @Override
    public void onDialFailed(Call call, String message, CallStatus callStatus) {
        this.addMessageToLog("onCallFailed: " + message);
    }

    @Override
    public void onCallFailed(Call call, String message, CallStatus callStatus) {
        this.addMessageToLog("onCallFailed: " + message);
    }

    @Override
    public void onMediaChangeRequested(final Call call, boolean hadAudio, boolean hadVideo) {
        this.addMessageToLog("onMediaChangeRequested : " + hadAudio + ", " + hadVideo);

        final Phone phone = ConfigFragment.UC.getPhone();

        // enable audio if the call has audio now, but didn't previously
        if (call.hasRemoteAudio() && !hadAudio)
            phone.enableLocalAudio(true);

        // if the call has video now whereas it didn't previously, prompt
        // the user to now enable video
        if (call.hasRemoteVideo() && !hadVideo) {
            this.getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder
                        .setTitle("Media Renegotiated")
                        .setMessage("The remote party has requested video - enable local video?")
                        .setCancelable(false)
                        .setPositiveButton("Enable video", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                addMessageToLog("User enabled local video");
                                phone.enableLocalVideo(true);
                            }
                        })
                        .setNegativeButton("", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                addMessageToLog("User selected to not enable local video");
                                // nothing further to be done
                            }
                        })
                        .create()
                        .show();
                }
            });
        }
    }

    @Override
    public void onStatusChanged(Call call, CallStatus callStatus) {
        this.addMessageToLog("onStatusChanged: " + callStatus);

        if (
                callStatus == CallStatus.ENDED ||
                callStatus == CallStatus.ERROR ||
                callStatus == CallStatus.BUSY) {

            // make the views invisible
            getView().findViewById(R.id.remote).setVisibility(View.INVISIBLE);
            getView().findViewById(R.id.local).setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onStatusChanged(Call call, CallStatusInfo callStatusInfo) {
        this.addMessageToLog("onStatusChanged: " + callStatusInfo);
    }

    @Override
    public void onRemoteDisplayNameChanged(Call call, String name) {
        this.addMessageToLog("onRemoteDisplayNameChanged: " + name);
    }

    @Override
    public void onRemoteMediaStream(Call call) {
        this.addMessageToLog("onRemoteMediaStream");
    }

    @Override
    public void onInboundQualityChanged(Call call, int i) {
        this.addMessageToLog("onInboundQualityChanged: " + i);
    }

    @Override
    public void onRemoteHeld(Call call) {
        this.addMessageToLog("onRemoteHeld");
    }

    @Override
    public void onRemoteUnheld(Call call) {
        this.addMessageToLog("onRemoteUnheld");
    }




    //////////////////////////////////////////
    //
    // PhoneListener methods

    public void onIncomingCall(final Call call) {
        this.addMessageToLog("onIncomingCall");
        final CallListener callListener = this;

        // prompt the user to accept or reject the call
        // remember that the UI stuff must be queued on the main
        // UI thread - so need to wrap code in a Runnable that
        // we schedule for execution on the main thread.
        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder
                    .setTitle("Incoming Call")
                    .setMessage("Incoming call from: " + call.getRemoteAddress())
                    .setCancelable(false)
                    .setPositiveButton("Answer", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            addMessageToLog("User accepted call");

                            // answer the call with audio and video
                            call.answer(audioCheckBox.isChecked(), videoCheckBox.isChecked());
                            call.addListener(callListener);

                            // make sure that the video surfaces are assigned
                            // note that while not all of the actions in setupVideoViews should be
                            // done following the call to answer the call, the call to set which
                            // camera to use should be done following accepting the call
                            setupVideoViews(getView());
                            call.setVideoView(remoteSurface);
                        }
                    })
                    .setNegativeButton("Reject", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            addMessageToLog("User rejected call");
                            call.end();
                        }
                    })
                    .create()
                    .show();
            }
        };

        // now queue on the main UI thread
        this.getActivity().runOnUiThread(runnable);
    }

    public void onCaptureSettingChange(PhoneVideoCaptureSetting vcs, int val) {
        this.addMessageToLog("onCaptureSettingChange");
    }

    public void onLocalMediaStream() {
        this.addMessageToLog("onLocalMediaStream");


    }
}
